﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

    public float upForce = 200f;
    public float flapDuration;
    public Sprite std_idle;
    public Sprite std_flap;
    public Sprite std_die;
    public Sprite red_eyes_idle;
    public Sprite red_eyes_flap;
    public Sprite red_eyes_die;

    private bool isDead = false;
    private Rigidbody2D rb2d;
    private Animator anim;
    private float flapTime = 0f;
    private Sprite idle;
    private Sprite flap;
    private Sprite die;

    //[SerializeField]
    public SpriteRenderer skin;

	// Use this for initialization
	void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        skin = GetComponent<SpriteRenderer>();
        switch(PlayerPrefs.GetInt("Skin"))
        {
            case (1):
                idle = std_idle;
                flap = std_flap;
                die = std_die;
                break;
            case (2):
                idle = red_eyes_idle;
                flap = red_eyes_flap;
                die = red_eyes_die;
                break;
        }
        skin.sprite = idle;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(isDead == false)
        {
            if(Input.GetMouseButtonDown(0))
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(0, upForce));
                flapTime = Time.time;
                skin.sprite = flap;
                //anim.SetTrigger("Flap");
            }
            if(flapTime != 0f && Time.time - flapTime >= flapDuration)
            {
                //anim.SetTrigger("Flap");
                skin.sprite = idle;
                flapTime = 0f;
            }

        }
	}

    void OnCollisionEnter2D()
    {
        rb2d.velocity = Vector2.zero;
        isDead = true;
        //anim.SetTrigger("Die");
        skin.sprite = die;
        GameControl.instance.BirdDied();
    }
}
