﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

    private Animator anim;
    public Slider sl;
    public Toggle tg;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenSettings()
    {
        anim.SetTrigger("Settings");
        sl.value = PlayerPrefs.GetFloat("Volume");
        if (PlayerPrefs.GetInt("Sound") == 1)
            tg.isOn = true;
        else
            tg.isOn = false;
    }

    public void CloseSettings()
    {
        anim.SetTrigger("SettingsBack");
        PlayerPrefs.SetFloat("Volume", sl.value);
        if(tg.isOn)
            PlayerPrefs.SetInt("Sound", 1);
        else
            PlayerPrefs.SetInt("Sound", 0);
    }

    public void ChangeSkin1()
    {
        PlayerPrefs.SetInt("Skin", 1);
    }

    public void ChangeSkin2()
    {
        PlayerPrefs.SetInt("Skin", 2);
    }
}
