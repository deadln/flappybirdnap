﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitConfirmation : MonoBehaviour {

    private Animator anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Quit()
    {
        anim.SetTrigger("ConfirmQuit");
    }

    public void QuitYes()
    {
        Application.Quit();
    }

    public void QuitNo()
    {
        anim.SetTrigger("ConfirmQuitBack");
    }
}
