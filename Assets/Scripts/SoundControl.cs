﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour {

    private AudioSource src;

	// Use this for initialization
	void Start () {
        src = GetComponent<AudioSource>();
        if (PlayerPrefs.GetInt("Sound") == 1)
            src.volume = PlayerPrefs.GetFloat("Volume");
        else
            src.volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
